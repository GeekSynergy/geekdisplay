package com.geeksynergy.geekdisplay;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;

@SuppressLint("Registered") // This is a parent Activity and can't exist by itself
public class ParentActivity extends AppCompatActivity {
    static String SharedPreferencesKey = "GeekDisplay";
    static String EXTRA_ADDRESS = "device_address";
}
