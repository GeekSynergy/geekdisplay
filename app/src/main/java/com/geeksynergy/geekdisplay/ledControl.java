package com.geeksynergy.geekdisplay;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.geeksynergy.geekdisplay.Fragment.SettingsFragment;
import com.geeksynergy.geekdisplay.Fragment.StopWatchFragment;
import com.geeksynergy.geekdisplay.Fragment.TimeFragment;
import com.geeksynergy.geekdisplay.Fragment.TimerFragment;

import java.io.IOException;
import java.util.UUID;

import static com.geeksynergy.geekdisplay.ParentActivity.SharedPreferencesKey;


public class ledControl extends ParentActivity {
    private ActionBar toolbar;
    Fragment fragment;
    String address = null;
    private ProgressDialog progress;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    private boolean isBtConnected = false;
    //SPP UUID. Look for it
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private SharedPreferences sharedpreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_led_control);
        Intent newint = getIntent();
        address = newint.getStringExtra(DeviceList.EXTRA_ADDRESS);
        if(address.equals("virtual")){
            new VirtualConnectBT().execute(); // call the class to show demo UI
        }
        else
        {
            new ConnectBT().execute(); //Call the class to connect

        }
        toolbar = getSupportActionBar();
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigationView);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        toolbar.setTitle("Geek Display");
        fragment = new SettingsFragment(btSocket);
        loadFragment(fragment);
    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener= new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.settings:
                    toolbar.setTitle("Geek Display");
                    fragment = new SettingsFragment(btSocket);
                    loadFragment(fragment);
                    return true;
                case R.id.time:
                    toolbar.setTitle("Time");
                    fragment = new TimeFragment(btSocket);
                    loadFragment(fragment);
                    return true;
                case R.id.timer:
                    toolbar.setTitle("Timer");
                    fragment = new TimerFragment(btSocket);
                    loadFragment(fragment);
                    return true;
                case R.id.stopwatch:
                    toolbar.setTitle("Stopwatch");
                    fragment = new StopWatchFragment(btSocket);
                    loadFragment(fragment);
                    return true;
                case R.id.aboutusbnav:
                    Intent intent = new Intent(ledControl.this,AboutUsActivity.class);
                    startActivity(intent);
                    return true;
            }
            return false;
        }
    };
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_led_control, menu);
//        MenuItem item = menu.findItem(R.id.darkswitch);
//        item.setActionView(R.layout.switch_layout);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.password) { //For reset password/ update password
            openDialog();
            return true;
        } if (id == R.id.disconnect) { //For Disconnect
            Disconnect();
            return true;
        }if (id == R.id.aboutUs) { //For About Us
            Intent intent = new Intent(ledControl.this,AboutUsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void openDialog() { // pop-up reset/update password dialog box
        final MaterialDialog.Builder materialDialog = new MaterialDialog.Builder(ledControl.this);
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.resetpage, null);
        final EditText oldPassword =  alertLayout.findViewById(R.id.old);
        final EditText newPassword =  alertLayout.findViewById(R.id.newp);
        final EditText conPassword =  alertLayout.findViewById(R.id.con);
        final Button send =  alertLayout.findViewById(R.id.send);
        final Button cancel =  alertLayout.findViewById(R.id.cancel);
        materialDialog.customView(alertLayout,false);
        materialDialog.cancelable(false);
        final MaterialDialog dialog = materialDialog.build();
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.show();
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { // user data send to display
                if(oldPassword.getText().toString().equals("")){
                    oldPassword.setError("Please Fillup Detail");
                }else if(newPassword.getText().toString().equals("")){
                    newPassword.setError("Please Fillup Detail");
                }else if(conPassword.getText().toString().equals("")){
                    conPassword.setError("Please Fillup Detail");
                }else{
                    if(!newPassword.getText().toString().equals(conPassword.getText().toString())){ //for password mismatch(new password and confirm password)
                        conPassword.setError("Mismatch");
                    }else{
                        if (btSocket!=null)
                        {
                            try
                            {
                                msg("uploading data ...");
                                //send data to diplay
                                btSocket.getOutputStream().write(("old~"+oldPassword.getText().toString()+",new~"+newPassword.getText().toString()).toString().getBytes());
                                int byteCount = btSocket.getInputStream().available();
                                if(byteCount > 0){
                                    //response back
                                    byte[] rawBytes = new byte[byteCount];
                                    btSocket.getInputStream().read(rawBytes);
                                    final String string=new String(rawBytes,"UTF-8");
                                    new Handler().post(new Runnable() {
                                        public void run(){
                                            if(string.replace(": ","").toString().equals("true")){ // if old password is match then only user can use new password
                                                dialog.cancel();
                                                Toast.makeText(ledControl.this,"Successfully change password and login with new password",Toast.LENGTH_LONG).show();
                                                Disconnect();
                                            }else {
                                                //old password mismatch response
                                                Toast.makeText(ledControl.this,"Old Password mismatch",Toast.LENGTH_LONG).show();
                                                oldPassword.setError("Please enter correct password");
                                            }
                                        }
                                    });

                                }else{
                                    // something wrong between display and app connection
                                    Toast.makeText(ledControl.this,"Something wrong happened, Please try again. ",Toast.LENGTH_LONG).show();
                                }
//
                            }
                            catch (IOException e)
                            {
                                msg("Error");
                            }
                        }
                    }
                }
            }
        });

    }
    private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute()
        {
            progress = ProgressDialog.show(ledControl.this, "Connecting...", "Please wait!!!");  //show a progress dialog
        }

        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            try
            {
                if (btSocket == null || !isBtConnected)
                {
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);//connects to the device's address and checks if it's available
                    btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();//start connection
                    fragment = new SettingsFragment(btSocket);
                    loadFragment(fragment);

                }
            }
            catch (IOException e)
            {
                ConnectSuccess = false;//if the try failed, you can check the exception here
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess)
            {
                msg("Connection Failed. Is it a SPP Bluetooth? Try again.");
                SharedPreferences pref = getApplicationContext().getSharedPreferences(SharedPreferencesKey, 0); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();
                editor.clear();
                editor.apply();
                Intent intent = new Intent(ledControl.this,DeviceList.class);
                startActivity(intent);
                finish();
                progress.dismiss();
            }
            else
            {

                passwordVerification();
                progress.dismiss();
            }

        }
    }
    private void passwordVerification() { //App password validation for accessing app ui and perform operation into display
        final MaterialDialog.Builder materialDialog = new MaterialDialog.Builder(ledControl.this);
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.password, null);
        final EditText password =  alertLayout.findViewById(R.id.password);
        final Button submit =  alertLayout.findViewById(R.id.submit);
        final Button cancel =  alertLayout.findViewById(R.id.cancel);
        materialDialog.customView(alertLayout,false);
        materialDialog.cancelable(false);
        final MaterialDialog dialog = materialDialog.build();
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.show();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(password.getText().toString().equals("") || password.getText().toString().equals(null)){
                    password.setError("Please Enter Password");
                }else{
                    try {
                        msg("verifying the password");
                        btSocket.getOutputStream().write(("password~"+password.getText().toString()).toString().getBytes());
//                        Toast.makeText(ledControl.this, "fpasss "  + btSocket.getInputStream().available(), Toast.LENGTH_SHORT).show();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                int byteCount = 0;
                                try {
                                    byteCount = btSocket.getInputStream().available();

                                    if (byteCount > 0) {
                                        byte[] rawBytes = new byte[byteCount];
                                        btSocket.getInputStream().read(rawBytes);
                                        Log.d("rawBytes", String.valueOf(rawBytes));
                                        final String string = new String(rawBytes, "UTF-8");
                                        new Handler().post(new Runnable() {
                                            public void run() {
                                                if (string.replace(": ", "").toString().equals("true")) {
                                                    msg("Connected.");
                                                    isBtConnected = true;
                                                    isBtConnected = true;
                                                    sharedpreferences = getSharedPreferences(SharedPreferencesKey, Context.MODE_PRIVATE);
                                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                                    editor.putString("address", address);
                                                    editor.putBoolean("isBtConnected", isBtConnected);
                                                    editor.commit();
                                                    dialog.dismiss();
                                                } else {
                                                    Toast.makeText(ledControl.this, "Password mismatch", Toast.LENGTH_LONG).show();
                                                    password.setError("Please enter correct password");
                                                }
                                            }
                                        });

                                    }else{
                                        Toast.makeText(ledControl.this,"Something wrong happened, Please try again. ",Toast.LENGTH_LONG).show();
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        },1000);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Disconnect();
            }
        });

    }
    private void Disconnect() // for disconnect bluetooth between led display and app
    {
        if (btSocket!=null) //If the btSocket is busy
        {
            try
            {
                btSocket.close(); //close connection
                //clear SharedPreferences value
                SharedPreferences pref = getApplicationContext().getSharedPreferences(SharedPreferencesKey, 0); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();
                editor.clear();
                editor.apply();
                Intent intent = new Intent(ledControl.this,DeviceList.class);
                startActivity(intent);
                finish();
            }
            catch (IOException e)
            { msg("Error");}
        }
        finish(); //return to the first layout

    }
    private class VirtualConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute()
        {
            progress = ProgressDialog.show(ledControl.this, "Virtually Connecting...", "Please wait!!!");  //show a progress dialog
        }

        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            ConnectSuccess = true; // we are forcefully making connection success to establish virtual connection
            return null;
        }
        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess)
            {
                msg("Connection Failed. Is it a SPP Bluetooth? Try again.");
                finish();
            }
            else
            {
                msg("Virtually device is Connected.");

            }
            progress.dismiss();
        }
    }
    private void msg(String s)
    {
        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
    }
}
