package com.geeksynergy.geekdisplay.FragmentDemo;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.geeksynergy.geekdisplay.R;
import com.geeksynergy.geekdisplay.SplashScreenActivity;
import com.geeksynergy.geekdisplay.ledControl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class TimeDemoFragment extends Fragment {
    ImageButton sendTime;
    private static BroadcastReceiver tickReceiver;
    TextView timeDigital;
    private CountDownTimer secCounter;
    private int sec;
    private String hr;
    private String min;
    private String secs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.time_fragment, container, false);
        timeDigital=view.findViewById(R.id.timeDigital);
        sendTime= view.findViewById(R.id.sendTime);
        sendTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateFormat df = new SimpleDateFormat("HH:mm:ss");
                Calendar calobj = Calendar.getInstance();
                Toast.makeText(getContext(),df.format(calobj.getTime()),Toast.LENGTH_SHORT).show();
            }
        });
        timeDigital.setText(String.valueOf(Calendar.getInstance().get(Calendar.HOUR))+":"+String.valueOf(Calendar.getInstance().get(Calendar.MINUTE))+":"+String.valueOf(Calendar.getInstance().get(Calendar.SECOND)));
        sec=Calendar.getInstance().get(Calendar.SECOND);
        secCounter=new CountDownTimer(59000, 1000) {
            public void onFinish() {
            }
            public void onTick(long millisUntilFinished) {
                if(Calendar.getInstance().get(Calendar.HOUR)<10){
                    hr="0"+String.valueOf(Calendar.getInstance().get(Calendar.HOUR));
                }else{
                    hr=String.valueOf(Calendar.getInstance().get(Calendar.HOUR));
                }
                if(Calendar.getInstance().get(Calendar.MINUTE)<10){
                    min="0"+String.valueOf(Calendar.getInstance().get(Calendar.MINUTE));
                }else{
                    min=String.valueOf(Calendar.getInstance().get(Calendar.MINUTE));
                }

                if(sec<10){
                    secs="0"+String.valueOf(sec);
                }else{
                    secs=String.valueOf(sec);
                }
                sec++;
                timeDigital.setText(hr+":"+min+":"+secs);
            }

        }.start();
        tickReceiver=new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().compareTo(Intent.ACTION_TIME_TICK)==0){
                    secCounter.cancel();
                    if(Calendar.getInstance().get(Calendar.HOUR)<10){
                        hr="0"+String.valueOf(Calendar.getInstance().get(Calendar.HOUR));
                    }else{
                        hr=String.valueOf(Calendar.getInstance().get(Calendar.HOUR));
                    }
                    if(Calendar.getInstance().get(Calendar.MINUTE)<10){
                       min="0"+String.valueOf(Calendar.getInstance().get(Calendar.MINUTE));
                    }else{
                        min=String.valueOf(Calendar.getInstance().get(Calendar.MINUTE));
                    }
                   timeDigital.setText(hr+":"+min+":0"+String.valueOf(Calendar.getInstance().get(Calendar.SECOND)));
                   sec=0;
                   secCounter.start();
                }

            }
        };

        //Register the broadcast receiver to receive TIME_TICK
        getActivity().registerReceiver(tickReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
        return view;
    }


}
