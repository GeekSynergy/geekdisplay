package com.geeksynergy.geekdisplay.FragmentDemo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.geeksynergy.geekdisplay.IntroControl;
import com.geeksynergy.geekdisplay.R;

import java.io.IOException;

public class SettingsDemoFragment extends Fragment {
    Spinner panel,direction;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_fragment, container, false);
//        RadioGroup radioGroup = (RadioGroup) view .findViewById(R.id.radiobtn11);
//        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                Toast.makeText(getContext(),"It's a Demo Page.\nThis functionality is not implemented hare.",Toast.LENGTH_LONG).show();
//            }
//        });
        panel= view.findViewById(R.id.panel);
        direction= view.findViewById(R.id.direction);
        final ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, android.R.id.text1);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        panel.setAdapter(spinnerAdapter);
        for(int panelNo=1;panelNo<5;panelNo++){
            spinnerAdapter.add(String.valueOf(panelNo));
            spinnerAdapter.notifyDataSetChanged();
        }

        final ArrayAdapter<String> spinnerAdapterForDiraction = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, android.R.id.text1);
        spinnerAdapterForDiraction.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        direction.setAdapter(spinnerAdapterForDiraction);
        spinnerAdapterForDiraction.add("Left");
        spinnerAdapterForDiraction.notifyDataSetChanged();
        spinnerAdapterForDiraction.add("Right");
        spinnerAdapterForDiraction.notifyDataSetChanged();

        return view;
    }
}
