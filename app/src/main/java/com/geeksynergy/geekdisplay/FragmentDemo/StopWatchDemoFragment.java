package com.geeksynergy.geekdisplay.FragmentDemo;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.geeksynergy.geekdisplay.R;

import java.util.Timer;
import java.util.TimerTask;

public class StopWatchDemoFragment extends Fragment {
    private static boolean isTimerRunning=false;
    Button update,reset;
    TextView millsec,time;
    Timer timer;
    TimerTask timerTask;
    final Handler handler = new Handler();
    int sec=0;
    int min=0;
    int hr=0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stopwatch_fragment, container, false);
        time=view.findViewById(R.id.time);
        update=view.findViewById(R.id.update);
        reset=view.findViewById(R.id.reset);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stoptimertask();
                time.setText("00:00:00");
                update.setText("Play");
                sec=0;
                min=0;
                hr=0;
                isTimerRunning=false;
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isTimerRunning==false){
                    isTimerRunning=true;
                    startTimer();
                }else{
                    stoptimertask();
                    update.setText("Play");
                    isTimerRunning=false;

                }

            }
        });
        return view;
    }

    public void startTimer() {
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, 1000, 1000); //
    }

    public void stoptimertask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
            timerTask.cancel();
            timerTask=null;
        }
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        update.setText("Pause");
                        String secString="";
                        String minString = "";
                        String hrString = "";
                        sec++;
                        if(sec>59){
                            sec=sec-60;
                            min=min+1;
                        }
                        if(min>59){
                            min=min-60;
                            hr=hr+1;
                        }
                        if(sec<10){
                            secString="0"+String.valueOf(sec);
                        }else{
                            secString=String.valueOf(sec);
                        }
                        if(min<10){
                            minString="0"+String.valueOf(min);
                        }else{
                            minString=String.valueOf(min);
                        }
                        if(hr<10){
                            hrString="0"+String.valueOf(hr);
                        }else{
                            hrString=String.valueOf(hr);
                        }
                        if(hr>999){
                            stoptimertask();

                        }else{
                            time.setText(String.valueOf(hrString)+":"+String.valueOf(minString)+":"+String.valueOf(secString));
                        }

                    }
                });
            }
        };
    }

}

