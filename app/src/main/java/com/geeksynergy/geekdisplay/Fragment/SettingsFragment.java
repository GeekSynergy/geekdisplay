package com.geeksynergy.geekdisplay.Fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.geeksynergy.geekdisplay.R;

import java.io.IOException;

@SuppressLint("ValidFragment")
public class SettingsFragment extends Fragment {
    private final BluetoothSocket btSocket;
    Button btnOn, btnOff,upload;
    EditText content;
    Switch ledswith,mood;
    SeekBar brightness;
    SeekBar scrollSpeed;
    TextView lumn;
    String address = null;
    private ProgressDialog progress;
    Spinner panel,direction;
    int flagCheck=0;
    int flagCheckDirection=0;

    public SettingsFragment(BluetoothSocket btSocket) {
        this.btSocket=btSocket;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_fragment, container, false);
        upload=(Button)view.findViewById(R.id.upload);
        content=(EditText) view.findViewById(R.id.editText2);
        mood=view.findViewById(R.id.mood);
        panel= view.findViewById(R.id.panel);
        direction= view.findViewById(R.id.direction);
        ledswith=(Switch)view.findViewById(R.id.ledswitch);
        brightness = (SeekBar)view.findViewById(R.id.seekBar);
        scrollSpeed = (SeekBar)view.findViewById(R.id.scrollseekBar);


        final ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, android.R.id.text1);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        panel.setAdapter(spinnerAdapter);
        for(int panelNo=1;panelNo<5;panelNo++){
            spinnerAdapter.add(String.valueOf(panelNo));
            spinnerAdapter.notifyDataSetChanged();
        }

        final ArrayAdapter<String> spinnerAdapterForDiraction = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, android.R.id.text1);
        spinnerAdapterForDiraction.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        direction.setAdapter(spinnerAdapterForDiraction);
        spinnerAdapterForDiraction.add("Left");
        spinnerAdapterForDiraction.notifyDataSetChanged();
        spinnerAdapterForDiraction.add("Right");
        spinnerAdapterForDiraction.notifyDataSetChanged();

        scrollSpeed.setProgress(25);
        brightness.setProgress(80);
        ledswith.setChecked(false); // by default test led is disable/ inactive
        direction.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (btSocket!=null) //checking connectivity
                {
                    if(flagCheckDirection!=0){
                        try
                        {
                            msg("Direction  Data Update");
                            if (btSocket!=null) {
                                if(position==0){
                                    btSocket.getOutputStream().write(("Direction~"+"Left").toString().getBytes()); //if inactive, change to active
                                }else if(position==1){
                                    btSocket.getOutputStream().write(("Direction~"+"Right").toString().getBytes()); //if inactive, change to active
                                }

                            }
                        }
                        catch (IOException e)
                        {
                            msg("Error while Direction data update");
                        }
                    }else{
                        flagCheckDirection++;
                    }

                }else{

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        panel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (btSocket!=null) //checking connectivity
                {
                    if(flagCheck!=0){
                        try
                        {
                            msg("Panel Data Update");
                            if (btSocket!=null) {
                                btSocket.getOutputStream().write(("Panel~"+String.valueOf(position+1)).toString().getBytes()); //if inactive, change to active
                            }
                        }
                        catch (IOException e)
                        {
                            msg("Error while panel data update");
                        }
                    }else{
                        flagCheck++;
                    }

                }else{

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ledswith.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() // change led test (active <-> inactive)
        {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {

                if(isChecked){
                    //switchStatus.setText("Switch is currently ON");
                    if (btSocket!=null) //checking connectivity
                    {
                        try
                        {
                            msg("swith on LED");
                            if (btSocket!=null) {
                                btSocket.getOutputStream().write("ON~".toString().getBytes()); //if inactive, change to active
                            }
                        }
                        catch (IOException e)
                        {
                            msg("Error while swith on LED");
                        }
                    }
                }else{
                    //switchStatus.setText("Switch is currently OFF");
                    if (btSocket!=null)
                    {
                        try
                        {
                            msg("swith off LED");
                            if (btSocket!=null) {
                                btSocket.getOutputStream().write("OFF~".toString().getBytes()); //if active, change to inactive
                            }
                        }
                        catch (IOException e)
                        {
                            msg("Error while swith off LED");
                        }
                    }
                }

            }
        });
        mood.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() // change led test (active <-> inactive)
        {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {

                if(isChecked){
                    //switchStatus.setText("Switch is currently ON");
                    if (btSocket!=null) //checking connectivity
                    {
                        try
                        {
                            msg("Changing Mood");
                            if (btSocket!=null) {
                                btSocket.getOutputStream().write(("Mood~"+"on").toString().getBytes()); //if inactive, change to active
                            }
                        }
                        catch (IOException e)
                        {
                            msg("Error while Changing Mood");
                        }
                    }
                }else{
                    //switchStatus.setText("Switch is currently OFF");
                    if (btSocket!=null)
                    {
                        try
                        {
                            msg("Changing Mood");
                            if (btSocket!=null) {
                                btSocket.getOutputStream().write(("Mood~"+"off").toString().getBytes()); //if active, change to inactive
                            }
                        }
                        catch (IOException e)
                        {
                            msg("Error while changing mood");
                        }
                    }
                }

            }
        });
        RadioGroup radioGroup = (RadioGroup) view .findViewById(R.id.radiobtn11);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected

                switch(checkedId) {
                    case R.id.radio_static:
//                        if (checked){
                            try
                            {
                                String style="Style~"+0;
                                msg("Display is changed to Static");
                                if (btSocket!=null) {
                                    btSocket.getOutputStream().write(style.getBytes()); //for static text
                                }

                            }
                            catch (IOException e)
                            {
                                msg("Error while sending display style");

                            }
                            break;
//                        }

                    case R.id.radio_scrolling:
//                        if (checked){
                            try
                            {
                                String style="Style~"+1;
                                msg("Display is changed to Scrolling");
                                if (btSocket!=null) {
                                    btSocket.getOutputStream().write(style.getBytes()); //for scrolling text
                                }

                            }
                            catch (IOException e)
                            {
                                msg("Error while sending display style");

                            }
                            break;
                }
            }
        });

        upload.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                uploadContent();      //method to send content to arduino.
            }
        });

        brightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { //change brightness state (increase amd decrease)
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser==true)
                {
                    // lumn.setText(String.valueOf(progress));

                    String bright="Brightness~"+String.valueOf(progress);
                    //msg(bright);
                    try
                    {
                        if(bright.length()<15){
                            if (btSocket!=null) {
                                btSocket.getOutputStream().write(bright.getBytes());
                            }

                        }
                    }
                    catch (IOException e)
                    {
                        msg("Error while sending brightness");

                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
             /*  msg("progress= "+seekBar.getProgress());
                msg("getMeasuredWidth= "+seekBar.getMeasuredWidth());
                msg("getMeasuredHeight= "+seekBar.getMeasuredHeight());
                msg("getScaleX= "+seekBar.getScaleX());
                msg("getScaleY= "+seekBar.getScaleY());
*/


            }
        });

        scrollSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { //change led display scroll speed (increase amd decrease)
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser==true)
                {
                    // lumn.setText(String.valueOf(progress));

                    String sSpeed="SSpeed~"+String.valueOf(progress);
                    //msg(bright);
                    try
                    {
                        if(sSpeed.length()<15){
                            btSocket.getOutputStream().write(sSpeed.getBytes());

                        }
                    }
                    catch (IOException e)
                    {
                        msg("Error while sending scroll speed");

                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        return view;
    }
    private  void uploadContent(){
        if (btSocket!=null)
        {
            try
            {
                msg("uploading data ...");
                btSocket.getOutputStream().write(("Content~"+content.getText()).toString().getBytes()); // update content for led display
                // msg(content.getText().toString());
                //msg(("Content~"+content.getText()).toString());
            }
            catch (IOException e)
            {
                msg("Error");
            }
        }
    }
    private void msg(String s)
    {
        Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();
    }

}
