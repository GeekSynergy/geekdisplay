package com.geeksynergy.geekdisplay.Fragment;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.geeksynergy.geekdisplay.R;

import java.io.IOException;

@SuppressLint("ValidFragment")
public class TimerFragment extends Fragment implements View.OnClickListener {
    private final BluetoothSocket btSocket;
    ImageButton update,clear;
    TextView h1,h2,m1,m2,s1,s2,n1,n2,n3,n4,n5,n6,n7,n8,n9,n0;
    View view;

    public TimerFragment(BluetoothSocket btSocket) {
        this.btSocket=btSocket;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.timer_fragment, container, false);
        h1=view.findViewById(R.id.h1);
        h2=view.findViewById(R.id.h2);
        m1=view.findViewById(R.id.m1);
        m2=view.findViewById(R.id.m2);
        s1=view.findViewById(R.id.s1);
        s2=view.findViewById(R.id.s2);
        n1=view.findViewById(R.id.n1);
        n2=view.findViewById(R.id.n2);
        n3=view.findViewById(R.id.n3);
        n4=view.findViewById(R.id.n4);
        n5=view.findViewById(R.id.n5);
        n6=view.findViewById(R.id.n6);
        n7=view.findViewById(R.id.n7);
        n8=view.findViewById(R.id.n8);
        n9=view.findViewById(R.id.n9);
        n0=view.findViewById(R.id.n0);
        update=view.findViewById(R.id.update);
        clear=view.findViewById(R.id.clear);
        n1.setOnClickListener(this);
        n2.setOnClickListener(this);
        n3.setOnClickListener(this);
        n4.setOnClickListener(this);
        n5.setOnClickListener(this);
        n6.setOnClickListener(this);
        n7.setOnClickListener(this);
        n8.setOnClickListener(this);
        n9.setOnClickListener(this);
        n0.setOnClickListener(this);
        update.setOnClickListener(this);
        clear.setOnClickListener(this);

        clear.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                s2.setText("0");
                s1.setText("0");
                m2.setText("0");
                m1.setText("0");
                h2.setText("0");
                h1.setText("0");
                return false;
            }
        });
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.n1:
                textInputReceiver(n1.getText().toString());
                break;
            case R.id.n2:
                textInputReceiver(n2.getText().toString());
                break;
            case R.id.n3:
                textInputReceiver(n3.getText().toString());
                break;
            case R.id.n4:
                textInputReceiver(n4.getText().toString());
                break;
            case R.id.n5:
                textInputReceiver(n5.getText().toString());
                break;
            case R.id.n6:
                textInputReceiver(n6.getText().toString());
                break;
            case R.id.n7:
                textInputReceiver(n7.getText().toString());
                break;
            case R.id.n8:
                textInputReceiver(n8.getText().toString());
                break;
            case R.id.n9:
                textInputReceiver(n9.getText().toString());
                break;
            case R.id.n0:
                textInputReceiver(n0.getText().toString());
                break;
            case R.id.update:
                updateIntoDisplay();
                break;
            case R.id.clear:
                clearText();
                break;
            default:
                break;
        }
    }

    private void updateIntoDisplay() {
        if(!(h1.getText().toString().equals("0") && h2.getText().toString().equals("0") &&
                m1.getText().toString().equals("0") && m2.getText().toString().equals("0") &&
                s1.getText().toString().equals("0") && s2.getText().toString().equals("0"))){

            int sec=Integer.parseInt(s1.getText().toString()+s2.getText().toString());
            int min=Integer.parseInt(m1.getText().toString()+m2.getText().toString());
            int hr=Integer.parseInt(h1.getText().toString()+h2.getText().toString());

            if(sec>59){
                sec=sec-60;
                min=min+1;
            }
            if(min>59){
                min=min-60;
                hr=hr+1;
            }
            if(hr>99){
                Toast.makeText(getContext(), "Our max capacity is 99:59:59", Toast.LENGTH_SHORT).show();
            }else{
                if(sec<10){
                    s1.setText("0");
                    s2.setText(String.valueOf(sec).substring(0,1));
                }else {
                    s1.setText(String.valueOf(sec).substring(0, 1));
                    s2.setText(String.valueOf(sec).substring(1, 2));
                }
                if(min<10){
                    m1.setText("0");
                    m2.setText(String.valueOf(min).substring(0, 1));
                }else {
                    m1.setText(String.valueOf(min).substring(0, 1));
                    m2.setText(String.valueOf(min).substring(1, 2));
                }
                if(hr<10){
                    h1.setText("0");
                    h2.setText(String.valueOf(hr).substring(0, 1));
                }else {
                    h1.setText(String.valueOf(hr).substring(0, 1));
                    h2.setText(String.valueOf(hr).substring(1, 2));
                }
                if (btSocket!=null) //checking connectivity
                {
                    try
                    {
                        msg("Uploading....");
                        String hourString,minString,secString;
                        if(hr<10){
                            hourString="0"+String.valueOf(hr);
                        }else{
                            hourString=String.valueOf(hr);
                        }
                        if(min<10){
                            minString="0"+String.valueOf(min);
                        }else{
                            minString=String.valueOf(min);
                        }
                        if(sec<10){
                            secString="0"+String.valueOf(sec);
                        }else{
                            secString=String.valueOf(sec);
                        }
                        btSocket.getOutputStream().write(("timer~"+hourString+":"+minString+":"+secString).toString().getBytes());
                    }
                    catch (IOException e)
                    {
                        msg("Error while uploading.");
                    }
                }
            }


        }else{
            Toast.makeText(getContext(), "Set Time & Try again", Toast.LENGTH_SHORT).show();
        }
    }

    private void clearText() {
        s2.setText(s1.getText().toString());
        s1.setText(m2.getText().toString());
        m2.setText(m1.getText().toString());
        m1.setText(h2.getText().toString());
        h2.setText(h1.getText().toString());
        h1.setText("0");
    }

    public void textInputReceiver(String input){
        if(input.equals("0")) {
            if (h1.getText().toString().equals("0") && h2.getText().toString().equals("0") &&
                    m1.getText().toString().equals("0") && m2.getText().toString().equals("0") &&
                    s1.getText().toString().equals("0") && s2.getText().toString().equals("0")) {

            }else{
                if(h1.getText().toString().equals("0")){
                    h1.setText(h2.getText().toString());
                    h2.setText(m1.getText().toString());
                    m1.setText(m2.getText().toString());
                    m2.setText(s1.getText().toString());
                    s1.setText(s2.getText().toString());
                    s2.setText(input);
                }
            }
        }else{
            if(h1.getText().toString().equals("0")){
                h1.setText(h2.getText().toString());
                h2.setText(m1.getText().toString());
                m1.setText(m2.getText().toString());
                m2.setText(s1.getText().toString());
                s1.setText(s2.getText().toString());
                s2.setText(input);
            }
        }
    }
    private void msg(String s)
    {
        Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();
    }
}
