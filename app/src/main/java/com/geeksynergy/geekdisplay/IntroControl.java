package com.geeksynergy.geekdisplay;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.geeksynergy.geekdisplay.FragmentDemo.SettingsDemoFragment;
import com.geeksynergy.geekdisplay.FragmentDemo.StopWatchDemoFragment;
import com.geeksynergy.geekdisplay.FragmentDemo.TimeDemoFragment;
import com.geeksynergy.geekdisplay.FragmentDemo.TimerDemoFragment;

public class IntroControl extends ParentActivity {

    private ActionBar toolbar;
    Fragment fragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_control);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar = getSupportActionBar();
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigationView);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        Toast.makeText(IntroControl.this,"It's a Demo Page.\nAll the functionality not implemented hare.",Toast.LENGTH_LONG).show();
        toolbar.setTitle("Geek Display");
        fragment = new SettingsDemoFragment();
        loadFragment(fragment);
    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener= new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.settings:
                    toolbar.setTitle("Geek Display");
                    fragment = new SettingsDemoFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.time:
                    toolbar.setTitle("Time");
                    fragment = new TimeDemoFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.timer:
                    toolbar.setTitle("Timer");
                    fragment = new TimerDemoFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.stopwatch:
                    toolbar.setTitle("Stopwatch");
                    fragment = new StopWatchDemoFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.aboutusbnav:
                        Intent intent = new Intent(IntroControl.this,AboutUsActivity.class);
                        startActivity(intent);
                        return true;
            }
            return false;
        }
    };
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_led_control, menu);
//        MenuItem item = menu.findItem(R.id.darkswitch);
//        item.setActionView(R.layout.switch_layout);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.password) { //For reset password/ update password
            Toast.makeText(IntroControl.this,"It's a Demo Page.\nThis functionality is not implemented hare.",Toast.LENGTH_LONG).show();
            return true;
        } if (id == R.id.disconnect) { //For Disconnect
            Toast.makeText(IntroControl.this,"It's a Demo Page.\nThis functionality is not implemented hare.",Toast.LENGTH_LONG).show();
            return true;
        }if (id == R.id.aboutUs) { //For Disconnect
            Intent intent = new Intent(IntroControl.this,AboutUsActivity.class);
            startActivity(intent);
            Toast.makeText(IntroControl.this,"It's a Demo Page.\nThis functionality is not implemented hare.",Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(IntroControl.this, DeviceList.class);
        startActivity(intent);
        finish();
    }
}
