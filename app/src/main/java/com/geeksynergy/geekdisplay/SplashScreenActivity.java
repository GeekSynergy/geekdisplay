package com.geeksynergy.geekdisplay;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import static com.geeksynergy.geekdisplay.ParentActivity.EXTRA_ADDRESS;
import static com.geeksynergy.geekdisplay.ParentActivity.SharedPreferencesKey;

public class SplashScreenActivity extends ParentActivity {
    private SharedPreferences prefResponse;
    private boolean isBtConnected;
    private String address;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        //auto redirect to different activity based on condition
        new CountDownTimer(3000, 1000) {
            public void onFinish() {
                prefResponse = getSharedPreferences(SharedPreferencesKey, 0);
                isBtConnected = prefResponse.getBoolean("isBtConnected", false);
                if (isBtConnected == true) {
                    address = prefResponse.getString("address", "");
                    if (!address.equals("")) { //if you are paired or connected
                        Intent i = new Intent(SplashScreenActivity.this, ledControl.class);
                        i.putExtra(EXTRA_ADDRESS, address);
                        startActivity(i);
                    } else { //if you are not paired or connected
                        jumpToDeviceList();
                    }
                } else{ //if you are not paired or connected
                    jumpToDeviceList();
                }
            }

            public void onTick(long millisUntilFinished) {
            }

        }.start();
    }

    private void jumpToDeviceList() { //if you are not paired or connected
        Intent startActivity = new Intent(SplashScreenActivity.this, DeviceList.class);
        startActivity(startActivity);
        finish();
    }
}
